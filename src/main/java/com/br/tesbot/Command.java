package com.br.tesbot;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class Command extends ListenerAdapter {

    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {

        String[] args = event.getMessage().getContentRaw().split("\\s+");
        
        if (!event.getAuthor().isBot()) {
            event.getChannel().sendTyping().queue();
            event.getChannel().sendMessage(args[0]).queue();
        }
        
    }
}
